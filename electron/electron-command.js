const { ipcMain, dialog, app, shell } = require('electron');
let fs = require('fs');
let xml2js = require('xml2js');
let parseString = require('xml2js').parseString;
const log = require('electron-log');

/*ipcMain.on('asynchronous-message', (event, arg) => {
  event.sender.send('asynchronous-reply', 'pong');
});*/

/**
 * get-user-path
 */
ipcMain.on('get-user-path', event => {
  log.info('get-user-path');
  event.returnValue = require('os').homedir();
});

ipcMain.on('open-m2', event => {
  log.info('open-m2');
  shell.openItem(require('os').homedir() + '/.m2/');
  event.returnValue = true;
});

/**
 * transform-xml-to-json
 */
ipcMain.on('transform-xml-to-json', (event, path) => {
  log.info('transform-xml-to-json');
  console.log(path);
  try {
    // path = app.getAppPath() + '/exampleXML/test-xml2json.json';
    let data = fs.readFileSync(path, { encoding: 'utf-8' });
    parseString(data, { explicitArray: false }, function(err, result) {
      json = JSON.stringify(result);
      event.returnValue = json;
      /*fs.writeFile(app.getAppPath() + '/exampleXML/test-xml2json.json', json, err => {
        if (err) {
          console.log('An error ocurred creating the file ' + err.message);
        }

        console.log('The file has been succesfully saved');
      });*/
    });
  } catch (err) {
    console.log(err);
    event.returnValue = false;
  }
});

/**
 * get-template-setting-json
 */
ipcMain.on('get-template-setting-json', event => {
  log.info('get-template-setting-json');
  try {
    let data = fs.readFileSync(app.getAppPath() + '/src/assets/documents/template-settings.json', { encoding: 'utf-8' });
    event.returnValue = data;
  } catch (err) {
    console.log(err);
    event.returnValue = false;
  }
});

/**
 * save-setting-json-to-xml
 */
ipcMain.on('save-setting-json-to-xml', (event, content) => {
  try {
    let path = content[0];
    let data = content[1];
    // path = app.getAppPath() + '/exampleXML/test-xml2json.json'
    //let data = fs.readFileSync(path, { encoding: 'utf-8' });

    // let dataJson = JSON.parse(data);
    // var obj = { name: 'Super', Surname: 'Man', age: '23' };
    let builder = new xml2js.Builder();
    let xml = builder.buildObject(data);
    console.log(xml);
    // path = app.getAppPath() + '/exampleXML/test-json2xml.xml'
    fs.writeFile(path, xml, err => {
      if (err) {
        console.log('An error ocurred creating the file ' + err.message);
        event.returnValue = false;
      } else {
        event.returnValue = true;
        console.log('The file has been succesfully saved');
      }
    });
  } catch (err) {
    console.log(err);
    event.returnValue = false;
  }
});

/**
 * load-files
 */
ipcMain.on('load-files', (event, arg) => {
  log.info('load-files');
  console.log('load-files', arg);
  try {
    var data = fs.readFileSync(arg, { encoding: 'utf-8' });
    event.returnValue = data;
  } catch (err) {
    log.error(err);
    event.returnValue = null;
  }
});

/**
 * save-file
 */
ipcMain.on('save-file', (event, content) => {
  console.log('save-file', content);

  dialog.showSaveDialog(fileName => {
    if (fileName === undefined) {
      console.log("You didn't save the file");
      return;
    }

    // fileName is a string that contains the path and filename created in the save file dialog.
    fs.writeFile(fileName, content, err => {
      if (err) {
        console.log('An error ocurred creating the file ' + err.message);
        event.returnValue = false;
      }

      console.log('The file has been succesfully saved');
    });
  });

  event.returnValue = true;
});

/**
 * saveXMLWithPath
 */
ipcMain.on('saveXMLWithPath', (event, content) => {
  const fileName = content[0];
  const contentXML = content[1];
  fs.writeFile(fileName, contentXML, err => {
    if (err) {
      console.log('An error ocurred creating the file ' + err.message);
    }

    console.log('The file has been succesfully saved');
  });
  event.returnValue = null;
});

/**
 * save-json
 */
ipcMain.on('save-json', (event, content) => {
  const fileName = content[0];
  const contentJson = content[1];
  fs.writeFile(fileName, contentJson, err => {
    if (err) {
      console.log('An error ocurred creating the file ' + err.message);
      event.returnValue = false;
    }

    console.log('The file has been succesfully saved');
  });

  event.returnValue = true;
});
