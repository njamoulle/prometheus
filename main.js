const { app, BrowserWindow } = require('electron');
const electronCmd = require('./electron/electron-command');
const log = require('electron-log');
const { autoUpdater } = require('electron-updater');

let win;

function createWindow() {
  autoUpdater.checkForUpdates();
  // autoUpdater.checkForUpdatesAndNotify();
  // autoUpdater.autoInstallOnAppQuit = true;
  console.log(app.getVersion());
  win = new BrowserWindow({
    width: 920,
    height: 800,
    backgroundColor: '#ffffff',
    icon: `file://${__dirname}/dist/assets/logo.png`
  });
  win.loadURL(`file://${__dirname}/dist/index.html`);
  // win.setMenuBarVisibility(false);
  win.setMinimumSize(920, 800);
  //// uncomment below to open the DevTools.
  // win.webContents.openDevTools();
  // Event when the window is closed.

  win.on('closed', function() {
    win = null;
  });
}

// Create window on electron intialization
app.on('ready', createWindow);
// Quit when all windows are closed.
app.on('window-all-closed', function() {
  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
app.on('activate', function() {
  // macOS specific close process
  if (win === null) {
    createWindow();
  }
});

function sendStatusToWindow(text) {
  log.info(text);
  console.log(text);
}

/*autoUpdater.setFeedURL({
    provider: "generic",
    url: "https://gitlab.com/_example_repo_/-/jobs/artifacts/master/raw/dist?job=build"
});*/

autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';
log.info('App starting...');

autoUpdater.on('checking-for-update', () => {
  sendStatusToWindow('Checking for update...');
});
autoUpdater.on('update-available', info => {
  sendStatusToWindow('Update available.');
  win.webContents.send('update-available', 0);
});
autoUpdater.on('update-not-available', info => {
  sendStatusToWindow('Update not available.');
});
autoUpdater.on('error', err => {
  sendStatusToWindow('Error in auto-updater. ' + err);
});
autoUpdater.on('download-progress', progressObj => {
  let log_message = 'Download speed: ' + progressObj.bytesPerSecond;
  log_message = log_message + ' - Downloaded ' + progressObj.percent + '%';
  log_message = log_message + ' (' + progressObj.transferred + '/' + progressObj.total + ')';
  win.webContents.send('update-progress', progressObj.percent);
  sendStatusToWindow(log_message);
});
autoUpdater.on('update-downloaded', info => {
  sendStatusToWindow('Update downloaded');
  autoUpdater.quitAndInstall();
});
