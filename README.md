version 0.6.0

### Nouveaux changements

Application :
- Retrait de la section Validation
- Changement du Settings.xml pour l'ajout de la nouvelle config-generale
- Ajout du filtre dans la config-generale

### Prochains changements

Application :
- Ajout de la page d'accueil
- Amélioration continue du visuel
- Ajout de la section Projets
- Mise en place du vérificateur de santé en local

### =====================

version 0.5.0

Application :
- Amelioration validation par config
- Correction réinitialisation garde user // password

### =====================

version 0.4.3

Application :
- Ajout Auto Updater
- Ajout Loading dialog + pourcentage
- Ajout tooltip pour les erreurs dans settings

### =====================

version 0.4.0

Settings.xml :
- Ajout Réinitialisation
- Ajout Changement Centrale
- Ajout Enregistrer

### =====================

Version 0.3.0

Validations
- Ajout Section Validations
