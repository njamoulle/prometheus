import { ValidationPropriete } from './validation-propriete';
export class ValidationProfile {
  public id: string;
  public proprietes: ValidationPropriete[] = [];

  constructor() {}

  public addValidationPropriete(validationPropriete: ValidationPropriete) {
    this.proprietes.push(validationPropriete);
  }
}
