import { SettingsPropriete } from './settings-propriete';
import { ValidationPropriete } from './validation';
import { ValidationService } from './../services/validation.service';

export class SettingsProfile {
  public id: string;
  public proprietes: SettingsPropriete[] = [];
  public error = false;
  public errorCount = 0;
  public validationPropriete: ValidationPropriete;

  constructor(profile, private validationService: ValidationService) {
    this.id = profile.id;

    for (const propertie in profile.properties) {
      if (profile.properties.hasOwnProperty(propertie)) {
        this.proprietes.push(new SettingsPropriete(propertie, profile.properties[propertie]));
      }
    }

    this.validationService.getValidation().subscribe(validationProfiles => {
      this.updateValidationPropriete();
    });
  }

  /**
   * Ceci permet d'aller chercher une propriete avec une clé
   *
   * @param {string} key
   * @returns
   * @memberof SettingsProfile
   */
  public getProprieteByKey(key: string): SettingsPropriete {
    for (let index = 0; index < this.proprietes.length; index++) {
      const propriete = this.proprietes[index];
      if (propriete.key === key) {
        return propriete;
      }
    }
    return null;
  }

  /**
   * Ceci permet de valider l'erreur d'une propriete
   *
   * @memberof SettingsProfile
   */
  public validProprieteError() {
    this.error = false;
    this.errorCount = 0;
    for (let index = 0; index < this.proprietes.length; index++) {
      const propriete = this.proprietes[index];

      if (propriete.error) {
        this.error = true;
        this.errorCount++;
      }
    }
  }

  public updateSettingsProprietes(settingsObject: Object) {
    for (let index = 0; index < this.proprietes.length; index++) {
      const propriete: SettingsPropriete = this.proprietes[index];
      settingsObject['properties'][propriete.key] = propriete.value;
    }
  }

  public updateCentrale(centrale: string) {
    for (let index = 0; index < this.proprietes.length; index++) {
      const settingsPropriete: SettingsPropriete = this.proprietes[index];
      switch (settingsPropriete.key) {
        case 'db2.schema':
        case 'envCentrale':
        case 'accesweb.schema':
          settingsPropriete.value = centrale;
          break;
        case 'lettreEnvCentrale':
          settingsPropriete.value = centrale.slice(2, 3);
          break;
        case 'envIMS':
          settingsPropriete.value = this.getEnvIMS(centrale);
          break;
        default:
          break;
      }
    }
  }

  private getEnvIMS(centrale: string): string {
    let envIMS: string;
    const centrale2digit = centrale.substring(0, 2);
    switch (centrale2digit) {
      case 'TD':
        envIMS = 'IMDV';
        break;
      case 'TV':
        envIMS = 'IMPI';
        break;
      case 'TA':
        envIMS = 'IMTA';
        break;
      case 'TK':
        envIMS = 'IMTK';
        break;
      default:
        envIMS = '';
        break;
    }
    return envIMS;
  }

  /**
   *  Ceci permet de mettre a jour les validations si on change les validations
   *
   * @private
   * @memberof SettingsProfile
   */
  private updateValidationPropriete() {
    for (let index = 0; index < this.proprietes.length; index++) {
      const propriete = this.proprietes[index];
      propriete.validationPropriete = this.validationService.getValidationProprieteByIdAndKey(this.id, propriete.key);
    }
  }
}
