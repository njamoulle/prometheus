import { SettingsProfile } from './settings-profile';
import { ValidationPropriete } from './validation';

export class SettingsPropriete {
  public key: string;
  public value: string;
  public color: string;
  public error = false;
  public errorValue: string;
  public errorColor = '#ff8c66';
  public defaultColor = 'transparent';
  public validationPropriete: ValidationPropriete;

  constructor(key: string, value: string) {
    this.key = key;
    this.value = value;
    this.color = this.defaultColor;
  }

  /**
   * Ceci permet de valider cette propriété
   *
   * @param {SettingsProfile[]} settingsProfileList
   * @memberof SettingsPropriete
   */
  public valider(settingsProfileList: SettingsProfile[]): any {
    this.color = this.defaultColor;
    this.error = false;

    if (this.validationPropriete) {
      for (let index = 0; index < this.validationPropriete.values.length; index++) {
        const element = this.validationPropriete.values[index];
        const validationValues = element.split('!');

        if (validationValues.length === 1) {
          this.validEqual(validationValues);
        } else if (validationValues.length > 1) {
          this.validMultiEqual(settingsProfileList, validationValues);
        }
      }
    }
    const styles = { 'background-color': this.color };
    return styles;
  }

  /**
   * Ceci permet de valider une validation de type Equal
   *
   * @private
   * @param {string} validationValues
   * @memberof SettingsPropriete
   */
  private validEqual(validationValues: string[]) {
    if (this.value !== validationValues[0]) {
      this.setHasError(validationValues[0]);
    }
  }

  /**
   * Ceci permet de valider une validation avec plusieurs value
   *
   * @private
   * @param {SettingsProfile[]} SettingsProfileList
   * @param {string[]} validationValues
   * @memberof SettingsPropriete
   */
  private validMultiEqual(settingsProfileList: SettingsProfile[], validationValues: string[]) {
    for (let i = 0; i < settingsProfileList.length; i++) {
      const settingsProfile = settingsProfileList[i];

      switch (this.key) {
        case 'envIMS':
          this.validEnvIMS(settingsProfile);
          break;
        case 'lettreEnvCentrale':
          this.validLettreEnvCentrale(settingsProfile, validationValues);
          break;
        default:
          if (settingsProfile.id === validationValues[0]) {
            const settingsPropriete: SettingsPropriete = settingsProfile.getProprieteByKey(validationValues[1]);
            if (settingsPropriete && settingsPropriete.value !== this.value) {
              this.setHasError(settingsPropriete.value);
            }
          }
          break;
      }
    }
  }

  /**
   * Ceci permet de valider envIMS
   *
   * @private
   * @param {SettingsProfile} settingsProfile
   * @memberof SettingsPropriete
   */
  private validEnvIMS(settingsProfile: SettingsProfile) {
    const propriete = settingsProfile.getProprieteByKey(this.key);

    if (propriete) {
      const centrale2digit = propriete.value.substring(0, 2);
      switch (centrale2digit) {
        case 'TD':
          if (settingsProfile.id !== 'IMDV') {
            this.setHasError('IMDV');
          }
          break;
        case 'TV':
          if (settingsProfile.id !== 'IMPI') {
            this.setHasError('IMPI');
          }
          break;
        case 'TA':
          if (settingsProfile.id !== 'IMTA') {
            this.setHasError('IMTA');
          }
          break;
        case 'TK':
          if (settingsProfile.id !== 'IMTK') {
            this.setHasError('IMTK');
          }
          break;

        default:
          break;
      }
    }
  }

  /**
   * Ceci permet de valider lettreEnvCentrale
   *
   * @private
   * @memberof SettingsPropriete
   */
  private validLettreEnvCentrale(settingsProfile: SettingsProfile, validationValues: string[]) {
    const propriete = settingsProfile.getProprieteByKey(validationValues[0]);

    if (propriete) {
      const centraleLastDigit = propriete.value.substring(+validationValues[1], 3);
      if (this.value !== centraleLastDigit) {
        this.setHasError(centraleLastDigit);
      }
    }
  }

  /**
   * Ceci permet d'activer l'erreur
   *
   * @private
   * @memberof SettingsPropriete
   */
  private setHasError(errorValue: string) {
    this.error = true;
    this.color = this.errorColor;
    this.errorValue = errorValue;
  }
}
