import { ValidationProfile, ValidationPropriete } from '../model/validation';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ElectronService } from 'ngx-electron';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';

@Injectable()
export class ValidationService {
  private subjectValidationProfiles: BehaviorSubject<ValidationProfile[]> = new BehaviorSubject<ValidationProfile[]>([]);
  private urlAccesweb = 'assets/documents/validation-accesweb.json';
  private urlAda = 'assets/documents/validation-ada.json';
  private urlAdp = 'assets/documents/validation-adp.json';
  private urlAme = 'assets/documents/validation-ame.json';
  private urlFctoel = 'assets/documents/validation-fctoel.json';
  private urlSrvam = 'assets/documents/validation-srvam.json';
  private urlSrvlot = 'assets/documents/validation-srvlot.json';
  private urlSrvpm = 'assets/documents/validation-srvpm.json';
  private urlWls = 'assets/documents/validation-wls.json';

  constructor(private electronService: ElectronService, private snackBar: MatSnackBar, private http: HttpClient) {
    this.loadFile(this.urlAccesweb);
    this.loadFile(this.urlAda);
    this.loadFile(this.urlAdp);
    this.loadFile(this.urlAme);
    this.loadFile(this.urlFctoel);
    this.loadFile(this.urlSrvam);
    this.loadFile(this.urlSrvlot);
    this.loadFile(this.urlSrvpm);
    this.loadFile(this.urlWls);
  }

  /**
   * Ceci permet de loader les fichiers de validations pour chaque config
   * 
   * @private
   * @param {string} url 
   * @memberof ValidationService
   */
  private loadFile(url: string): void {
    if (this.electronService.isElectronApp) {
      const rawData = this.electronService.ipcRenderer.sendSync('load-files', this.electronService.remote.app.getAppPath() + '/src/' + url);
      const data: any = JSON.parse(rawData);
      this.subjectValidationProfiles.getValue().push(data);
      this.subjectValidationProfiles.next(this.subjectValidationProfiles.getValue());
    } else {
      this.http.get(url).subscribe((data: ValidationProfile) => {
        this.subjectValidationProfiles.getValue().push(data);
        this.subjectValidationProfiles.next(this.subjectValidationProfiles.getValue());
      });
    }
  }

  /**
   * Ceci permet de sauvegarder une version du Validation.json qui est dans le dossier documents
   * @returns
   * @memberof ValidationService
   */
  public save() {
    if (!this.electronService.isElectronApp) {
      return;
    }

    // NEED WORK AFTER REFACTER

    this.subjectValidationProfiles.next(this.subjectValidationProfiles.getValue());

    const success = this.electronService.ipcRenderer.sendSync('save-json', [
      this.electronService.remote.app.getAppPath() + this.urlAccesweb,
      JSON.stringify(this.subjectValidationProfiles.getValue())
    ]);

    if (success) {
      this.snackBar.openFromComponent(SnackBarComponent, {
        duration: 800,
        data: 'Validation a été changé',
        verticalPosition: 'top',
        panelClass: 'snack-bar-background'
      });
    }
  }

  public getValidation(): Observable<ValidationProfile[]> {
    return this.subjectValidationProfiles.asObservable();
  }

  public getProfileById(profileId: string): ValidationProfile {
    const profiles = this.subjectValidationProfiles.getValue();
    for (let index = 0; index < profiles.length; index++) {
      const profile = profiles[index];
      if (profile.id === profileId) {
        return profile;
      }
    }
    console.log('Prometheus a aucune validation pour le profile :: ', profileId);
    return null;
  }

  public getValidationProprieteByIdAndKey(profileId: string, key: string): ValidationPropriete {
    const validationProfile = this.getProfileById(profileId);
    if (!validationProfile) {
      return null;
    }

    const proprietes = validationProfile.proprietes;
    for (let index = 0; index < proprietes.length; index++) {
      const propriete = proprietes[index];
      if (propriete.key === key) {
        return propriete;
      }
    }

    console.log('Prometheus trouve pas de validation pour la clé et le profile suivant : ', key, profileId);
    return null;
  }
}
