import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ConfigPrometheus } from '../model';
import { ElectronService } from 'ngx-electron';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';

@Injectable()
export class ConfigPrometheusService {
  private subjectConfigPrometheus: BehaviorSubject<ConfigPrometheus> = new BehaviorSubject<ConfigPrometheus>(new ConfigPrometheus());
  private configPrometheusUrl = '/prometheus.json';

  constructor(private electronService: ElectronService, private snackBar: MatSnackBar) {
    if (this.electronService.isElectronApp) {
      const appPath = this.electronService.remote.app.getAppPath();
      const configJSON = this.electronService.ipcRenderer.sendSync('load-files', appPath + this.configPrometheusUrl);
      if (configJSON) {
        this.subjectConfigPrometheus = new BehaviorSubject<ConfigPrometheus>(JSON.parse(configJSON));
      } else {
        let settingPath = this.electronService.ipcRenderer.sendSync('get-user-path');
        settingPath = settingPath + '/.m2/settings.xml';
        setTimeout(() => {
          this.changeSettingPath(settingPath);
        }, 0);
      }
    } else {
      this.subjectConfigPrometheus.next(new ConfigPrometheus());
    }
  }

  public changeSettingPath(nouveauPath: string) {
    if (this.subjectConfigPrometheus.getValue().settingUrl === nouveauPath) {
      return;
    }

    const config = new ConfigPrometheus();
    config.settingUrl = nouveauPath;
    this.subjectConfigPrometheus.next(config);

    this.electronService.ipcRenderer.sendSync('save-json', [this.configPrometheusUrl, JSON.stringify(config)]);

    this.snackBar.openFromComponent(SnackBarComponent, {
      duration: 800,
      data: 'Prometheus Setting a été changé',
      verticalPosition: 'top',
      panelClass: 'snack-bar-background'
    });
  }

  public getConfigPrometheus(): Observable<ConfigPrometheus> {
    return this.subjectConfigPrometheus.asObservable();
  }
}
