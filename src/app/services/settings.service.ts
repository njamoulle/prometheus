import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ConfigPrometheusService } from './config-prometheus.service';
import { ElectronService } from 'ngx-electron';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SettingsService {
  private subjectSettingObject: BehaviorSubject<Object> = new BehaviorSubject<Object>({});
  private userSettings: any;
  private templateSettingsJsonUrl = 'assets/documents/template-settings.json';
  private settings: any;
  private userName: string;
  private userPassword: string;

  constructor(
    private electronService: ElectronService,
    private http: HttpClient,
    private configPrometheusService: ConfigPrometheusService
  ) {
    this.configPrometheusService.getConfigPrometheus().subscribe(configPrometheus => {
      if (this.electronService.isElectronApp) {
        if (!configPrometheus.settingUrl) {
          return;
        }
        // FOR MAKE JSON FILE
        /*const appPath = this.electronService.remote.app.getAppPath();
        console.log(
          this.electronService.ipcRenderer.sendSync('transform-xml-to-json', appPath + '\\src\\assets\\documents\\template-settings.xml')
        );*/
        this.userSettings = this.electronService.ipcRenderer.sendSync('transform-xml-to-json', configPrometheus.settingUrl);
        this.settings = this.electronService.ipcRenderer.sendSync('get-template-setting-json');
        this.userSettings = JSON.parse(this.userSettings);

        if (this.userSettings['settings']) {
          this.userName = this.userSettings['settings'].servers.server.username;
          this.userPassword = this.userSettings['settings'].servers.server.password;
        }

        this.settings = JSON.parse(this.settings);
        this.mergeRecursive(this.settings, this.userSettings);

        this.subjectSettingObject.next(this.settings);
      } else {
        // FOR TEST LOCAL
        /*this.http.get('assets/documents/nic-settings.json').subscribe(userSettings => {
          this.userSettings = userSettings;

          this.http.get(this.templateSettingsJsonUrl).subscribe(settings => {
            this.settings = settings;
            this.mergeRecursive(this.settings, this.userSettings);
            this.subjectSettingObject.next(this.settings);
          });
        });*/

        this.http.get(this.templateSettingsJsonUrl).subscribe(settings => {
          this.subjectSettingObject.next(settings);
        });
      }
    });
  }

  /**
   * Ceci permet d'écouter les changements au fichier settings
   * @returns Observable<Object>
   * @memberof SettingsService
   */
  public getSettings(): Observable<Object> {
    return this.subjectSettingObject.asObservable();
  }

  /**
   * Ceci permet de remettre le template comme settings
   * @memberof SettingsService
   */
  public reinitialisationSettings() {
    if (this.electronService.isElectronApp) {
      this.settings = this.electronService.ipcRenderer.sendSync('get-template-setting-json');
      this.settings = JSON.parse(this.settings);
      this.settings['settings'].servers.server.username = this.userName;
      this.settings['settings'].servers.server.password = this.userPassword;
      this.subjectSettingObject.next(this.settings);
    } else {
      this.http.get(this.templateSettingsJsonUrl).subscribe(settings => {
        this.subjectSettingObject.next(settings);
      });
    }
  }

  /**
   * Ceci permet de merger deux fichier .json
   * @returns Object
   * @memberof SettingsService
   */
  public mergeRecursive(mergeObjet1, objet2) {
    for (const propriete in objet2) {
      if (objet2[propriete].constructor === Object) {
        mergeObjet1[propriete] = this.mergeRecursive(mergeObjet1[propriete], objet2[propriete]);
      } else {
        if (objet2[propriete].length > 0 && typeof objet2[propriete] !== 'string') {
          for (let index = 0; index < objet2[propriete].length; index++) {
            if (objet2[propriete][index].constructor === Object) {
              let found = false;
              for (let index2 = 0; index2 < mergeObjet1[propriete].length; index2++) {
                if (objet2[propriete][index].id === mergeObjet1[propriete][index2].id) {
                  found = true;
                  mergeObjet1[propriete][index2] = this.mergeRecursive(mergeObjet1[propriete][index2], objet2[propriete][index]);
                }
              }

              if (!found) {
                mergeObjet1[propriete].push(objet2[propriete][index]);
              }
            } else {
              mergeObjet1[propriete][index] = objet2[propriete][index];
            }
          }
        } else {
          mergeObjet1[propriete] = objet2[propriete];
        }
      }
    }
    return mergeObjet1;
  }
}
