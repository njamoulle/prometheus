import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { DialogConfigComponent } from './../../dialog/dialog-config/dialog-config.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() clickMenu: EventEmitter<any> = new EventEmitter();

  constructor(public matDialog: MatDialog) {}

  ngOnInit() {}

  /**
   * Ceci permet daller chercher le url du setting.xml si prometheusConfig.json n'est pas la
   * @memberof AppComponent
   */
  public ouvrirFichierSetting() {
    this.matDialog.open(DialogConfigComponent, {
      width: '475px',
      height: '200px'
    });
  }

  public onMenu() {
    this.clickMenu.emit(null);
  }
}
