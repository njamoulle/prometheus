import { Component, OnInit } from '@angular/core';

const packageVersion = require('./../../../../package.json').version;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public version: string;
  constructor() {
    this.version = packageVersion;
  }

  ngOnInit() {}
}
