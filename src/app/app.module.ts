import { AccueilTabComponent, SettingsComponent, SettingsValidationComponent } from './tabs';
import { ConfigPrometheusService, SettingsService, ValidationService } from './services';
import { DialogAddPropertieComponent, DialogChangeCentraleComponent, DialogConfigComponent, DialogConfirmationComponent } from './dialog';
import { FooterComponent, HeaderComponent } from './ui';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { DialogLoadingComponent } from './dialog/dialog-loading/dialog-loading.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MarkdownModule } from 'ngx-markdown';
import { MaterialModule } from './material.module';
import { NgModule } from '@angular/core';
import { NgxElectronModule } from 'ngx-electron';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './ui/sidebar/sidebar.component';
import { SnackBarComponent } from './snack-bar/snack-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    DialogConfigComponent,
    SnackBarComponent,
    DialogAddPropertieComponent,
    DialogConfirmationComponent,
    DialogChangeCentraleComponent,
    DialogLoadingComponent,
    SettingsComponent,
    SettingsValidationComponent,
    SidebarComponent,
    AccueilTabComponent
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxElectronModule,
    RouterModule.forRoot(AppRoutes),
    MarkdownModule.forRoot()
  ],
  entryComponents: [
    DialogConfigComponent,
    SnackBarComponent,
    DialogAddPropertieComponent,
    DialogConfirmationComponent,
    DialogChangeCentraleComponent,
    DialogLoadingComponent
  ],
  providers: [ConfigPrometheusService, ValidationService, SettingsService],
  bootstrap: [AppComponent]
})
export class AppModule {}
