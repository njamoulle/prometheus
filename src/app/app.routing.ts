import { AccueilTabComponent, SettingsComponent, SettingsValidationComponent } from './tabs';

import { Routes } from '@angular/router';

export const AppRoutes: Routes = [
  { path: '', redirectTo: 'accueil', pathMatch: 'full' },
  { path: 'accueil', component: AccueilTabComponent },
  { path: 'settings-profil', component: SettingsComponent },
  { path: 'settings-validation', component: SettingsValidationComponent }
];
