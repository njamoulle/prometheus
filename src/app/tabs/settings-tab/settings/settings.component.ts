import { AfterViewChecked, Component, HostListener, OnInit } from '@angular/core';
import { ConfigPrometheus, SettingsProfile } from './../../../model';
import { ConfigPrometheusService, SettingsService, ValidationService } from './../../../services';
import { MatDialog, MatSnackBar } from '@angular/material';

import { DialogChangeCentraleComponent } from './../../../dialog';
import { ElectronService } from 'ngx-electron';
import { SnackBarComponent } from './../../../snack-bar/snack-bar.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, AfterViewChecked {
  public settings: Object;
  public settingsProfiles: SettingsProfile[] = [];

  public filtreDefault = [
    'envIMS',
    'db2.schema',
    'envCentrale',
    'oracle.schema',
    'lettreEnvCentrale',
    'serveur.id',
    'notes.serveur',
    'domain.host',
    'application.portDESJMOV',
    'application.hostDESJMOV',
    'application.envSOA',
    'application.portDESJWEB',
    'application.hostDESJWEB',
    'application.portSRVICD',
    'application.hostSRVICD'
  ];
  public filtreAccesweb = [
    'application.hostAccesWeb',
    'application.hostAWACADIE',
    'application.hostAWCA',
    'application.portAccesWeb',
    'application.portAccesWeb.https',
    'application.portAccesWeb.debug',
    'sitemetier.ada',
    'sitemetier.adp'
  ];
  public filtreAda = [
    'application.hostADA',
    'application.hostADAACADIE',
    'application.portADA',
    'application.portADA.https',
    'application.portADA.debug'
  ];
  public filtreAdp = [
    'application.hostADP',
    'application.hostADPACADIE',
    'application.hostADPCA',
    'application.portADP',
    'application.portADP.https',
    'application.portADP.debug'
  ];
  public filtreAme = ['application.hostAME', 'application.portAME', 'application.portAME.https', 'application.portAME.debug'];
  public filtreFctoel = [
    'application.hostFCTOEL',
    'application.hostFCTOELACADIE',
    'application.hostFCTOELCA',
    'application.portFCTOEL',
    'application.portFCTOEL.https',
    'application.portFCTOEL.debug'
  ];
  public filtreSrvam = ['application.hostSRVAM', 'application.portSRVAM', 'application.portSRVAM.https', 'application.portSRVAM.debug'];
  public filtreSrvlot = [
    'application.hostSRVLOT',
    'application.portSRVLOT',
    'application.portSRVLOT.https',
    'application.portSRVLOT.debug'
  ];
  public filtreSrvpm = ['application.hostSRVPM', 'application.portSRVPM', 'application.portSRVPM.https', 'application.portSRVPM.debug'];
  public filtreTout = this.filtreDefault.concat(
    this.filtreAccesweb,
    this.filtreAda,
    this.filtreAdp,
    this.filtreAme,
    this.filtreFctoel,
    this.filtreSrvam,
    this.filtreSrvlot,
    this.filtreSrvpm
  );
  public proprietefiltre = this.filtreTout;
  private configPrometheus: ConfigPrometheus;

  constructor(
    public electronService: ElectronService,
    private snackBar: MatSnackBar,
    private validationService: ValidationService,
    private settingsService: SettingsService,
    private configPrometheusService: ConfigPrometheusService,
    private matDialog: MatDialog
  ) {
    this.configPrometheusService.getConfigPrometheus().subscribe(configPrometheus => {
      this.configPrometheus = configPrometheus;
    });

    this.settingsService.getSettings().subscribe(settings => {
      this.settings = settings;
      this.settingsProfiles = [];
      try {
        for (let index = 0; index < settings['settings'].profiles.profile.length; index++) {
          const profile = settings['settings'].profiles.profile[index];
          if (profile.id.indexOf('config-generale') !== -1 || profile.id.indexOf('config-wls') !== -1) {
            this.settingsProfiles.push(new SettingsProfile(profile, this.validationService));
          }
        }
      } catch (e) {
        return;
      }
    });
  }

  ngOnInit() {}

  ngAfterViewChecked() {
    for (let index = 0; index < this.settingsProfiles.length; index++) {
      const settignsProfile = this.settingsProfiles[index];
      settignsProfile.validProprieteError();
    }
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.ctrlKey && event.keyCode === 19) {
      this.saveXml();
    }
  }

  public filtreProprietes(profile: SettingsProfile) {
    if (profile.id === 'config-generale') {
      const proprieteList = [];
      for (let index = 0; index < profile.proprietes.length; index++) {
        const propriete = profile.proprietes[index];
        if (this.proprietefiltre.indexOf(propriete.key) !== -1) {
          proprieteList.push(propriete);
        }
      }
      return proprieteList;
    } else {
      return profile.proprietes;
    }
  }

  public ouvrirM2() {
    this.electronService.ipcRenderer.sendSync('open-m2');
  }

  public reinitialisation() {
    this.settingsService.reinitialisationSettings();
  }

  public changementCentrale() {
    const dialogRef = this.matDialog.open(DialogChangeCentraleComponent, { disableClose: false });
    dialogRef.afterClosed().subscribe(centrale => {
      if (centrale) {
        for (let index = 0; index < this.settingsProfiles.length; index++) {
          const settingsProfile: SettingsProfile = this.settingsProfiles[index];
          settingsProfile.updateCentrale(centrale);
        }
        this.notification('Centrale mise a jour');
      }
    });
  }

  /**
   * Ceci permet de sauvegarder le xml via electron
   * @memberof AppComponent
   */
  public saveXml() {
    if (this.electronService.isElectronApp) {
      const basePathSettingUrl = this.configPrometheus.settingUrl.slice(0, this.configPrometheus.settingUrl.indexOf('.xml'));
      const backupSettingXmlUrl = basePathSettingUrl + '_backup.xml';

      const fichierBackup = this.electronService.ipcRenderer.sendSync('load-files', backupSettingXmlUrl);
      if (!fichierBackup) {
        this.electronService.ipcRenderer.sendSync('save-setting-json-to-xml', [backupSettingXmlUrl, this.settings]);
      }

      const profiles = this.settings['settings'].profiles.profile;
      for (let profileIndex = 0; profileIndex < profiles.length; profileIndex++) {
        for (let index = 0; index < this.settingsProfiles.length; index++) {
          const settingsProfile: SettingsProfile = this.settingsProfiles[index];

          if (profiles[profileIndex].id === settingsProfile.id) {
            settingsProfile.updateSettingsProprietes(profiles[profileIndex]);
          }
        }
      }

      const sauvegardeReussi = this.electronService.ipcRenderer.sendSync('save-setting-json-to-xml', [
        this.configPrometheus.settingUrl,
        this.settings
      ]);

      if (sauvegardeReussi) {
        this.notification('Setting est enregistré');
      }
    }
  }

  private notification(titre: string): void {
    this.snackBar.openFromComponent(SnackBarComponent, {
      duration: 800,
      data: titre,
      verticalPosition: 'top',
      panelClass: 'snack-bar-background'
    });
  }
}
