import { Component, OnInit } from '@angular/core';
import { DialogAddPropertieComponent, DialogConfirmationComponent } from './../../../dialog';
import { MatButton, MatDialog } from '@angular/material';
import { ValidationProfile, ValidationPropriete } from './../../../model/validation';

import { ValidationService } from './../../../services';

@Component({
  selector: 'app-settings-validation',
  templateUrl: './settings-validation.component.html',
  styleUrls: ['./settings-validation.component.scss']
})
export class SettingsValidationComponent implements OnInit {
  public validationProfiles: ValidationProfile[];

  constructor(private validationService: ValidationService, private matDialog: MatDialog) {
    this.validationService.getValidation().subscribe(validationProfileList => {
      this.validationProfiles = validationProfileList;
    });
  }

  ngOnInit() {}

  public addPropriete(validationProfile: ValidationProfile): void {
    const dialogRef = this.matDialog.open(DialogAddPropertieComponent, { disableClose: true });
    dialogRef.afterClosed().subscribe(validationPropriete => {
      if (validationPropriete) {
        validationProfile.proprietes.push(validationPropriete);
      }
    });
  }

  public addValue(validationPropriete: ValidationPropriete): void {
    const newString = 'new';
    validationPropriete.values.push(newString);
  }

  public removePropriete(list: any, index: number, closeButton: MatButton): void {
    const dialogRef = this.matDialog.open(DialogConfirmationComponent, { disableClose: false });
    dialogRef.afterClosed().subscribe(boolean => {
      if (boolean) {
        list.splice(index, 1);
      }

      closeButton._elementRef.nativeElement.blur();
    });
  }

  public saveValidationJSON(): void {
    this.validationService.save();
  }

  /**
   * Ceci sert a faire un fix avec le ngModel
   * Pour faire en sorte que la modification affecte seulement un element de la liste
   * @param {number} index
   * @param {number} value
   * @returns
   * @memberof AppComponent
   */
  public trackByIndex(index: number, value: number) {
    return index;
  }
}
