import { Component, OnInit } from '@angular/core';

import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'app-accueil-tab',
  templateUrl: './accueil-tab.component.html',
  styleUrls: ['./accueil-tab.component.scss']
})
export class AccueilTabComponent implements OnInit {
  public readme: string;
  private readmeUrl = '/README.md';

  constructor(private electronService: ElectronService) {}

  ngOnInit() {
    if (this.electronService.isElectronApp) {
      const appPath = this.electronService.remote.app.getAppPath();
      this.readme = this.electronService.ipcRenderer.sendSync('load-files', appPath + this.readmeUrl);
    } else {
      this.readme = 'Readme pas loader en local';
    }
  }
}
