import { Component, OnInit } from '@angular/core';

import { DialogLoadingComponent } from './dialog/dialog-loading/dialog-loading.component';
import { ElectronService } from 'ngx-electron';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public currentMenu = 0;
  constructor(private electronService: ElectronService, private matDialog: MatDialog) {}

  public ngOnInit() {
    if (this.electronService.isElectronApp) {
      this.electronService.ipcRenderer.on(
        'update-available',
        function(boolean) {
          this.matDialog.open(DialogLoadingComponent, { disableClose: true });
        }.bind(this)
      );
    } else {
      setTimeout(() => {
        this.matDialog.open(DialogLoadingComponent, { disableClose: false });
      }, 500);
    }
  }

  public changeMenu(menuOptionId: number) {
    this.currentMenu = menuOptionId;
    console.log('changeMenu', menuOptionId);
  }
}
