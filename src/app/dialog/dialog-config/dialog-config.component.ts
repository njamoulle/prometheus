import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { ConfigPrometheus } from './../../model';
import { ConfigPrometheusService } from './../../services';
import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'app-dialog-config',
  templateUrl: './dialog-config.component.html',
  styleUrls: ['./dialog-config.component.scss']
})
export class DialogConfigComponent implements OnInit {
  public settingPath: string;
  @ViewChild('inputSetting') inputSetting: any;

  constructor(
    public dialogRef: MatDialogRef<DialogConfigComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private configPrometheusService: ConfigPrometheusService,
    private electronService: ElectronService
  ) {}

  ngOnInit() {
    this.configPrometheusService.getConfigPrometheus().subscribe((configPrometheus: ConfigPrometheus) => {
      this.settingPath = configPrometheus.settingUrl;
      this.inputSetting.nativeElement.focus();
    });
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  public onGetSettingUrl() {
    const settingPath = this.electronService.remote.dialog.showOpenDialog({
      properties: ['openFile']
    })[0];
    this.configPrometheusService.changeSettingPath(settingPath);
    this.onNoClick();
  }

  public onBlur(settingPathValue: string) {
    this.configPrometheusService.changeSettingPath(settingPathValue);
  }
}
