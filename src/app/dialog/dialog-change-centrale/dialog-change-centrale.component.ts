import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-dialog-change-centrale',
  templateUrl: './dialog-change-centrale.component.html',
  styleUrls: ['./dialog-change-centrale.component.scss']
})
export class DialogChangeCentraleComponent implements OnInit {
  public centrale: string;

  constructor(public dialogRef: MatDialogRef<DialogChangeCentraleComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {}

  public onNoClick(): void {
    this.dialogRef.close(null);
  }

  public save(): void {
    this.dialogRef.close(this.centrale);
  }
}
