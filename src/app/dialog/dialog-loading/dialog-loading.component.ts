import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'app-dialog-loading',
  templateUrl: './dialog-loading.component.html'
})
export class DialogLoadingComponent implements OnInit {
  public pourcentage = 0;

  constructor(
    public dialogRef: MatDialogRef<DialogLoadingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private electronService: ElectronService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    if (this.electronService.isElectronApp) {
      this.electronService.ipcRenderer.on('update-progress', (event, pourcentage) => {
        this.pourcentage = Math.ceil(pourcentage);
        this.changeDetectorRef.detectChanges();
      });
    } else {
      setInterval(() => {
        this.pourcentage++;
      }, 500);
    }
  }

  public onNoClick(): void {}
}
