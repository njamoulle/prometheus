import { Component, Inject, Input, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ValidationProfile, ValidationPropriete } from './../../model/validation';

@Component({
  selector: 'app-dialog-add-propertie',
  templateUrl: './dialog-add-propertie.component.html',
  styleUrls: ['./dialog-add-propertie.component.scss']
})
export class DialogAddPropertieComponent implements OnInit {
  public settingPath: string;
  public validationPropriete: ValidationPropriete;
  @Input() validationProfile: ValidationProfile;

  constructor(public dialogRef: MatDialogRef<DialogAddPropertieComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    this.validationPropriete = new ValidationPropriete();
    this.validationPropriete.comment = 'new';
    this.validationPropriete.key = 'new';
    this.validationPropriete.values = ['new'];
  }

  public onNoClick(): void {
    this.dialogRef.close(null);
  }

  public save(): void {
    this.dialogRef.close(this.validationPropriete);
  }
}
